libemail-mime-encodings-perl (1.317-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.317.
  * Update upstream email address.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 Jan 2023 19:38:13 +0100

libemail-mime-encodings-perl (1.316-1) unstable; urgency=medium

  * Import upstream version 1.316.
  * Update copyright years
  * Update debhelper-compat to 13
  * Drop obsolete build-dependencies on Capture::Tiny (test rewritten) and
    Test::More (in core)
  * Declare compliance with Debian Policy 4.6.1

 -- Florian Schlichting <fsfs@debian.org>  Mon, 21 Nov 2022 22:02:43 +0100

libemail-mime-encodings-perl (1.315-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libemail-mime-encodings-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 20:48:32 +0100

libemail-mime-encodings-perl (1.315-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 21:26:40 +0100

libemail-mime-encodings-perl (1.315-2) unstable; urgency=medium

  * Team upload.

  [ Florian Schlichting ]
  * Fix upstream copyright holder information in debian/copyright.
  * Fix alternative build dependency to libtest-simple-perl.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Sat, 17 Feb 2018 18:20:16 +0200

libemail-mime-encodings-perl (1.315-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/watch: remove obsolete comment.

  [ Florian Schlichting ]
  * Import Upstream version 1.315
  * Add build-dependencies on Capture::Tiny and Test::More 0.96, drop
    Test::Pod and Test::Pod::Coverage
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0, directly
    link GPL-1+, update upstream contact and copyright holder)
  * Bump dh compatibility to level 8 (no changes necessary)
  * Switch to source format 3.0 (quilt)
  * Switch to short-form debian/rules
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Tue, 20 Aug 2013 21:55:32 +0200

libemail-mime-encodings-perl (1.313-1) unstable; urgency=low

  [ Brian Cassidy ]
  * New upstream release
  * debian/compat: now 7
  * debian/control:
    + Added myself to Uploaders
    + Standards-Version now 3.8.1
    + debhelper dep to 7
  * debian/rules: update to new standards
  * debian/copyright: update to new standards

  [ gregor herrmann ]
  * debian/control:
    - switch Vcs-Browser field to ViewSVN
    - add ${misc:Depends} to Depends: field
    - remove leading space and article from short description
    - mention module name in long description

 -- Brian Cassidy <brian.cassidy@gmail.com>  Thu, 30 Apr 2009 09:42:47 -0300

libemail-mime-encodings-perl (1.311-3) unstable; urgency=low

  [ gregor herrmann ]
  * debian/rules: delete /usr/lib/perl5 only if it exists (Closes: 467826).

  [ Ernesto Hernández-Novich (USB) ]
  * Upgraded to debhelper 6

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Mon, 14 Jan 2008 10:48:09 -0430

libemail-mime-encodings-perl (1.311-2) unstable; urgency=low

  [ Ernesto Hernández-Novich (USB) ]
  * Moved package into Debian Pkg Perl Project SVN.
  * Fixed copyright file with a better URL.
  * Clean up debian/rules.
  * Added Build-Depends-Indep on libtest-pod-perl and
    libtest-pod-coverage-perl.
  * Fixed watch file.
  * Fixed Maintainer field in control file.
  * Fixed copyright and control file with a better URL.

  [ Damyan Ivanov ]
  * Drop debian/docs
    + do not install redundant README
    + Changes is installed by dh_installchangelogs already
  * Bump debhelper compat level to 5
  * Make debian/watch use http://search.cpan.org/dist/
  * Put me instead of Ernesto in Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Mon, 03 Dec 2007 21:45:37 +0200

libemail-mime-encodings-perl (1.311-1) unstable; urgency=low

  * New upstream release.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Mon, 02 Jul 2007 11:17:46 -0400

libemail-mime-encodings-perl (1.310-1) unstable; urgency=low

  * New upstream release.
  * Cleaned up debian/rules.
  * Updated Standards Version.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Sat, 04 Nov 2006 17:04:58 -0400

libemail-mime-encodings-perl (1.3-1) unstable; urgency=low

  * Initial Release.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Mon, 05 Dec 2005 10:34:48 -0400
